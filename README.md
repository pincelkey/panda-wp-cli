# Description
``pandawp-cli`` is a command line which allow install quickly a Panda WP Starter Theme

# Install CLI 💻
```sh
npm install -g pandawp-cli
```

# Create new project 🐼
```sh
pandawp-cli -c <project>
pandawp-cli --create <project>
```

# License
MIT
