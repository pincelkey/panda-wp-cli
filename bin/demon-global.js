#!/usr/bin/env node

const { program }   = require('commander');
const shell         = require("shelljs");
const chalk         = require('chalk');
const readline      = require('readline');
const replace       = require("replace");
const fs            = require('fs');
const pjson         = require('../package.json');

program
    .option('-c, --create <project>', 'create a panda-wp theme')
    .option('-i, --install <project>')
    .version(pjson.version, '-v, --vers', 'output the current version');

program.parse(process.argv);

const options = program.opts();
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

if (options.create) {
    const themeName = options.create;

    if (!fs.existsSync(`./${themeName}`)) {
        console.log(chalk.green.bold("---------------------------------------------"));
        console.log(chalk.green.bold(">>> 1. Clone Panda WP repository from GitHub |"));
        console.log(chalk.green.bold("---------------------------------------------"));

        shell.exec(`git clone https://github.com/pincelkey/panda-wp.git ${themeName}`);
        shell.cd(`${themeName}/`);

        console.log(chalk.green.bold("---------------------------------------------"));
        console.log(chalk.green.bold(">>> 2. Instalando dependecias COMPOSER       |"));
        console.log(chalk.green.bold("---------------------------------------------"));

        shell.exec('composer install');
        shell.exec('cp .env.example .env');

        console.log(chalk.green.bold("---------------------------------------------"));
        console.log(chalk.green.bold(">>> 3. Instalando dependecias NPM (Vue)      |"));
        console.log(chalk.green.bold("---------------------------------------------"));

        shell.cd('resources/vue');
        shell.exec('npm install');
        shell.cd('../..');

        console.log(chalk.green.bold(""));
        console.log(chalk.green.bold("|------------------Question------------------|"));
        console.log(chalk.green.bold(""));

        console.log(chalk.green.bold("---------------------------------------------"));
        console.log(chalk.green.bold(">>> 4. Instalando dependecias NPM (Admin)    |"));
        console.log(chalk.green.bold("---------------------------------------------"));

        shell.cd('resources/admin');
        shell.exec('npm install');
        shell.cd('../..');

        settingFrontend(2, themeName);
    } else {
        console.log('Ther is another project with the same name, ¡please change it!');
        process.exit();
    }
}

function settingFrontend(count, themeName) {
    console.log(chalk.green.bold("---------------------------------------------"));
    console.log(chalk.green.bold(`>>> ${count === 2 ? '5' : '4'}. Configurando ambientes frontend       |`));
    console.log(chalk.green.bold("---------------------------------------------"));

    shell.exec('cp resources/vue/.env.example resources/vue/.env');

    if (count === 2)
        shell.exec('cp resources/admin/src/config.example.json resources/admin/src/config.json');

    console.log(chalk.green.bold(""));
    console.log(chalk.green.bold("|------------------Question------------------|"));
    console.log(chalk.green.bold(""));

    rl.question('> Hostname: ', (hostname) => {
        settingVueEnviroment(hostname, themeName);

        if (count === 2)
            settingAdminEnviroment(hostname, themeName);

        console.log(chalk.green.bold("------------------------------------------------"));
        console.log(chalk.green.bold(">>> Final. Building project in development mode |"));
        console.log(chalk.green.bold("------------------------------------------------"));

        shell.exec('npm run vue:stage');

        if (count === 2)
            shell.exec('npm run admin:stage');

        console.log(chalk.green.bold("                                                    "));
        console.log(chalk.green.bold("                               _                    "));
        console.log(chalk.green.bold("                              | |                   "));
        console.log(chalk.green.bold("         _ __   __ _ _ __   __| | __ _              "));
        console.log(chalk.green.bold("        | '_ \\ / _` | '_ \\ / _` |/ _` |             "));
        console.log(chalk.green.bold("        | |_) | (_| | | | | (_| | (_| |             "));
        console.log(chalk.green.bold("        | .__/ \\__,_|_| |_|\\__,_|\\__,_|             "));
        console.log(chalk.green.bold("        | |                                         "));
        console.log(chalk.green.bold("        |_|                                         "));
        console.log(chalk.green.bold('                                                    '));
        console.log(chalk.green.bold('       ¡Panda WP instalado exitosamente!            '));
        console.log(chalk.green.bold('                                                    '));
        console.log(chalk.green.bold('                                                    '));

        shell.exec('rm -Rf .git');

        process.exit();
    });
}

function settingVueEnviroment(hostname, theme) {
    replace({
        regex: "pandawp.site",
        replacement: hostname,
        paths: ['resources/vue/.env'],
        recursive: true,
        silent: true,
    });

    replace({
        regex: "pandawp",
        replacement: theme,
        paths: ['resources/vue/.env'],
        recursive: true,
        silent: true,
    });

    shell.exec('cp resources/vue/.env resources/vue/.env.staging');

    replace({
        regex: "NODE_ENV=development",
        replacement: 'NODE_ENV=production',
        paths: ['resources/vue/.env.staging'],
        recursive: true,
        silent: true,
    });

    replace({
        regex: "VUE_APP_MODE='development'",
        replacement: 'VUE_APP_MODE=\'staging\'',
        paths: ['resources/vue/.env.staging'],
        recursive: true,
        silent: true,
    });
}

function settingAdminEnviroment(hostname, theme) {
    replace({
        regex: "pandawp.site",
        replacement: hostname,
        paths: ['resources/admin/src/config.json'],
        recursive: true,
        silent: true,
    });

    replace({
        regex: "pandawp",
        replacement: theme,
        paths: ['resources/admin/src/config.json'],
        recursive: true,
        silent: true,
    });
}