# Development
1. Open this folder on ``command line``
2. Go to previous folder 
```sh
cd ..
```

3. Run this command
```sh
npm install -g /panda-wp-cli
```

4. Open new terminal and test command
```sh
panda-wp-cli -v
```